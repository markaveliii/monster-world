#ifndef _BATTLE_
#define _BATTLE_
#include <iostream>
#include <ctime>
#include "character.h"
#include "monster.h"
#include "player.h"

class battle
{
	public:
		battle();
		battle(monster&,player&);
		int gen_num();
		int pick_char();
		int init_bSeq(character&,character&);
	private:
		monster * m;
		player * p;
};

#endif
