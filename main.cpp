#include <iostream>
#include "monster.h"
#include "player.h"
#include "battle.h"

using namespace std;

int main()
{
	char name[100] = "Mark";
	char cont;
	int action;
	char response;
	monster Goblin(100,100);
	player P1(name,100,100);
	battle *B1 = new battle(Goblin,P1);
	if(B1)
	{
		do{
		cout << "Stats: \n\n";
		cout << "Goblin:\n";
		Goblin.display();
		cout << "\nPlayer:\n";
		P1.display();
		cout << "Do you want to attack?: ";
		cin >> response; cin.ignore(100,'\n');
		if(response == 'y' || response == 'Y')
			B1->init_bSeq(P1,Goblin);
		}while(response == 'y' || response == 'Y');
	}

	return 0;
}
