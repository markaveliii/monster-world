#include "monster.h"

monster::monster()
{
	HP = 0;
	MP = 0;
}

monster::monster(int H, int M): 
	character(H,M){}

monster::monster(const monster &cpy)
{
	HP = cpy.HP;
	MP = cpy.MP;
}

void monster::display()
{
	std::cout << "HP: " << HP << std::endl;
	std::cout << "MP: " << MP << std::endl;
}
