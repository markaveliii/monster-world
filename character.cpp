#include "character.h"
#include <iostream>

character::character()
{
	HP=100;
	MP=100;
}
character::character(int H, int M)
{
	HP = H;
	MP = M;
}

bool character::inc_HP(const int H)
{
	if(H==0)
		return false;
	HP+=H;
	return true;
}
int character::dec_HP(const int H)
{
	if(HP==0|| H==0)return 0;
	HP-=H;
	if(HP <= 0) return -1;
	else return 1;
}
bool character::inc_MP(const int){return true;}
bool character::dec_MP(const int){return true;}

bool character::set_HP(int H){HP = H; return true;}
int character::get_HP(){return HP;}
bool character::set_MP(int M){MP = M; return true;}
int character::get_MP(){return MP;}
