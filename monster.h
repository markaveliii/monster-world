#ifndef _MONSTER_
#define _MONSTER_
#include <iostream>
#include "character.h"
class monster: public character
{
	public:
		monster();
		monster(int,int);
		monster(const monster&);

		void display();
};

#endif
