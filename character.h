#ifndef _CHARACTER_
#define _CHARACTER_

class character
{
	public:
		character();
		character(int,int);

		bool inc_HP(const int);
		int dec_HP(const int);
		bool inc_MP(const int);
		bool dec_MP(const int);

		bool set_HP(int);
		int get_HP();
		int get_MP();
		bool set_MP(int);

		virtual void display() = 0;

		
	protected:
		int HP;
		int MP;
};
#endif
