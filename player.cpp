#include "player.h"

player::player(){}
player::player(char *name, int H, int M):
	character(H,M)
{
	ID = new char[strlen(name) +1];
	strcpy(ID,name);
}
player::player(const player &cpy)
{
	HP = cpy.HP;
	MP = cpy.MP;
	ID = new char[strlen(cpy.ID)+1];
	strcpy(ID,cpy.ID);
}

void player::display()
{
	std::cout << "ID: " << ID << std::endl;
	std::cout << "HP: " << HP << std::endl;
	std::cout << "MP: " << MP << std::endl;
}
