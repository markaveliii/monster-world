#ifndef _PLAYER_
#define _PLAYER_
#include <iostream>
#include "character.h"
#include <cstring>

class player: public character
{
	public:
		player();
		player(char *,int,int);	
		player(const player&);

		void display();
	protected:
		char *ID;
};
#endif
