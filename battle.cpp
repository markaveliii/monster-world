#include "battle.h"

battle::battle(){}
battle::battle(monster &mon, player &pla)
{
	m = new monster(mon);
	p = new player(pla);
}

int battle::gen_num()
{
	srand(time(0));	
	int num = rand()%10+1;	
	return num;
}

int battle::pick_char()
{
	int n = gen_num();
	if(n%2 == 0)
		return 1;
	else
		return -1;
}

int battle::init_bSeq(character &P, character &M)
{
	if(M.get_HP() == 0 || P.get_HP() == 0)
		return 0;
	int dmg = gen_num();
	int choice = pick_char();	
	if(choice == 1)
	{
		P.dec_HP(dmg);
		if(P.get_HP() <= 0)
		{
			P.set_HP(0);
			return -1;
		}
		return 2;		
	}

	if(choice == -1)
	{
		M.dec_HP(dmg);
		if(M.get_HP() <= 0)
		{
			M.set_HP(0);
			return 1;
		}
		return 2;
	}
	return 2;
}
